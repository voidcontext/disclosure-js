module.exports = (grunt) -> 
  grunt.initConfig 
    nodeunit:
      all: ['tests/*.js']
    coffee:
      compile:
        files: 
          'lib/disclosure-js-server.js' : 'src/disclosure-js-server.coffee'
          'lib/disclosure-js-api.js' : 'src/disclosure-js-api.coffee'
          'lib/ch.js' : 'src/connection-handler.coffee'
          'tests/test-disclosure-js-api.js' : 'src/test-disclosure-js-api.coffee'
    watch:
      coffee:
        files: '**/*.coffee'
        tasks: ['coffee:compile']

  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-nodeunit"



  