_version = 1

winston = require 'winston'

class DisclosureJSApi
  constructor: (@clients, @storage, @crypto) ->
  routes: () ->
    self = @
    generateusertoken: (req, res)->
      self.handleAPIRequest req, res, (req, res)->
        self.storage().get self.getUniqueId(req), (err,reply)->
          if reply
            self.jsonResponse res, userToken : reply
          else
            self.genToken self.getUniqueId(req), res

    getusertoken: (req, res)->
      self.handleAPIRequest req,res, (req, res)->
        self.storage().get self.getUniqueId(req), (err,reply)->
          self.jsonResponse res, userToken : reply

    publish: (req, res)->
      self.handleAPIRequest req,res, (req, res)->
        channel = req.body.channel
        matches = channel.match /^user_id:(.*)$/
        if matches
          channel = self.createUniqueId req.body.__app_secret__, matches[1]
        self.storage().publish channel, req.body.message
        self.jsonResponse res, {}
    version: (req, res) ->
      self.jsonResponse res, {version: _version}

  handleAPIRequest : (req,res, callback) ->
  	# check that we got an app secret, and we have to validate it too
    secret = req.body.__app_secret__
    if secret? and @clients[secret]?
      #when everything is ok, we can continue with the given callback
      callback req, res
    else
      # else cannot perform the current request
      res.writeHead 403, 'Content-Type': 'text/plain'
      res.end 'Not authorized'

  jsonResponse : (res,obj) ->
  	# send a json response
    res.writeHead 200, 'Content-Type': 'application/json'
    res.end JSON.stringify obj

  getUniqueId: (req) ->
    @createUniqueId req.body.__app_secret__, req.body.user_id
  createUniqueId: (secret,user_id) ->
    client = @clients[secret]
    client.id + '_' + user_id
  genToken : (uniqueId,res)->
    self = @
    # generate a random unique token
    storage = @storage()
    @crypto.randomBytes 48, (ex,buf) ->
      token = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-')
      storage.get token, (err,reply) ->
        if reply
          # while the token isn't unqiue we have to regenerate it
          self.genToken uniqueId,res
        else
          # store the token in redis
          storage.set token, uniqueId
          #storage.expire token, self.config.tokenExpire
          storage.set uniqueId, token
          # and send back to the token
          winston.info "UserToken added for " + uniqueId
          self.jsonResponse res, userToken : token

module.exports.init = (secret, storage, crypto) ->
  new  DisclosureJSApi secret, storage, crypto

  
      

