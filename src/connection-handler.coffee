redis = require 'redis'

class ConnectionHandler
  connections: {}
  constructor: (@config, @io) ->
    self = @
    # configure socket.io
    io.configure ()->
      # set authorization
      redisClient = redis.createClient config.redis.port, config.redis.host
      io.set 'authorization', (handshakeData,callback)->
        if handshakeData.query.token
          redisClient.get handshakeData.query.token, (err, reply) ->
            # when the user's secret is in redis then we trust him as an authenticated user
            if reply
              callback null, true
            else
              # unauthenticated user
              callback null, false
        else
          # no secret were given
          callback 'Bad URL'
      io.set 'log level', 1 

    # @TODO: create separeta namespaces as: /notificaions, /chat etc...
    io.sockets.on 'connection', (socket)->
      token = socket.manager.handshaken[socket.id].query.token

      _redisClient = redis.createClient config.redis.port,config.redis.host
      # get unique id
      uniqueId = _redisClient.get token, (err, uniqueId) ->
        # subscribe to the user's own channel
        _redisClient.subscribe uniqueId
        # subscribe to the broadcast channel
        _redisClient.subscribe 'broadcast'
        
        # when the redis client gets a message from the subscribed channels, we are sending back to the user's browser via socket.io
        _redisClient.on 'message', (channel,message)->
          socket.emit 'notify', JSON.parse message 

      # TODO: subscribe to group channels (a.k.a rooms)
  renewToken : (oldToken) ->



module.exports = ConnectionHandler

