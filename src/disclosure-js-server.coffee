createServer = (config) ->
  redis = require 'redis'
  express = require 'express'
  crypto = require 'crypto'
  ch = require __dirname + '/ch.js'

  # express app creation and settings
  app = express()
  app.use express.bodyParser()

  # api class initialization
  api = require(__dirname + '/disclosure-js-api.js').init config.clients, () ->
      redis.createClient config.redis.port, config.redis.host
    , crypto

  # set routes
  app.post '/generateusertoken', api.routes().generateusertoken
  app.post '/getusertoken', api.routes().getusertoken
  app.post '/publish', api.routes().publish
  app.get '/version', api.routes().version

  # create server and socket.io
  server = require('http').createServer app
  io = require('socket.io').listen server, 'origins' : "*:*"

  connectionHandler = new ch config,io

  server.listen config.port , null

module.exports = createServer








