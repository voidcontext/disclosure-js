
class FakeRes 
  message: null
  code: null
  init: ()->
    @code = null
    @message = null
    @
  writeHead: (code) ->
    @code = code
  end: (message) ->
    @message = message

class FakeReq
  headers:
    host: 'localhost'

  body:
    user_id : 1
    __app_secret__ : 'testSecret'
    channel: 'test-channel'
    message: 'message'

class Storage
  _d : {}
  _m : {}
  set: (key, value) ->
    @_d[key] = value
  get: (key, callback) ->
  	(callback) null, @_d[key]
  publish: (channel, message) ->
    @_m[channel] = message

storage = new Storage
api = require('../lib/disclosure-js-api.js').init 'testSecret' : { 'id' : 1, 'name' : 'test-client' }, () -> 
    storage
  , randomBytes: (num, callback) -> 
    callback null, new Date()
  , tokenExpire : 1

exports.testRoutes = (test) ->
  test.ok typeof api.routes() is 'object'
  test.ok typeof api.routes().generateusertoken isnt 'undefined'
  test.ok typeof api.routes().getusertoken isnt 'undefined'
  test.ok typeof api.routes().publish isnt 'undefined'
  test.ok typeof api.routes().version isnt 'undefined'

  req = new FakeReq()
  res = new FakeRes()

  api.routes().version req, res.init()
  test.ok res.code is 200
  test.ok((JSON.parse res.message).version is 1)

  api.routes().generateusertoken req, res.init()
  test.ok res.code is 200
  test.ok((JSON.parse res.message).userToken?)

  api.routes().getusertoken req, res.init()
  test.ok res.code is 200
  test.ok((JSON.parse res.message).userToken?)

  api.routes().publish req, res.init()
  test.ok res.code is 200
  test.ok(storage._m['test-channel']?);
  test.equal(storage._m['test-channel'], 'message');

  test.done()