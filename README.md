# disclosure-js

Socket.io based API server for sending and receiving realtime notifications from other webapps

[![Build Status](https://secure.travis-ci.org/voidcontext/disclosure-js.png)](http://travis-ci.org/voidcontext/disclosure-js)

## install

```
$ npm install -g disclosure-js
```
